# Luksman

![image alt text](screenshot.png "luksman screenshot")

A simple LUKS container manager through the CLI. Disclaimer: I did not create this project, but I did modify its code to improve usability. Credits for the original code go to an online member of a Matrix room.

## Install

>chmod +x install.sh

>chmod +x luksman

>sudo sh install.sh

## Uninstall

>chmod +x uninstall.sh

>sudo sh uninstall.sh

## Usage

Script can be ran by simply typing "luksman" after installation.
